export interface Message {
  content: Buffer;
  fields: {
    deliveryTag: number;
    exchange: string;
    redelivered: boolean;
    routingKey: string;
  };
  properties: {};
}

export namespace MockMessage {
  export const forBuffer = (routingKey: string, buffer: Buffer, properties?: Partial<{}>): Message => {
    return {
      content: buffer,
      fields: {
        deliveryTag: Math.floor(Math.random() * 10000),
        exchange: '',
        redelivered: false,
        routingKey,
      },
      properties: {
        appId: undefined,
        clusterId: undefined,
        contentEncoding: undefined,
        contentType: undefined,
        correlationId: undefined,
        deliveryMode: undefined,
        expiration: undefined,
        headers: {},
        messageId: undefined,
        priority: undefined,
        replyTo: undefined,
        timestamp: undefined,
        type: undefined,
        userId: undefined,
        ...properties,
      },
    };
  };

  export const empty = (routingKey: string, properties?: Partial<{}>): Message => {
    return forBuffer(routingKey, Buffer.alloc(0), properties);
  };
}
