import { compose, Middleware, Next } from './compose';

type Path = string | RegExp;

class Route<Message> {
  private readonly check: Path;
  constructor(
    public readonly path: Path,
    public readonly middleware: Middleware<Message>,
  ) {
    // if path is a string containing wildcards or has parameters, then convert to a regular expression
    if (typeof path === 'string') {
      if (path.includes('*') || path.startsWith(':') || path.includes('.:')) {
        // replace wildcards
        const wildcards = path.replace(/[.+?^${}()|[\]\\]/g, '\\$&').replace(/\*/g, '.*');
        // extract parameters
        const params = wildcards.replace(/(((^:)|((\\\.):))([^\\.]+))/g, '$5(?<$6>[^.]+)');
        this.check = new RegExp(`^${params}$`);
      } else {
        this.check = path;
      }
    } else {
      this.check = path;
    }
  }

  public match(routingKey: string): [boolean, Params] {
    if (typeof this.check === 'string') {
      return [this.check === routingKey, {}];
    }
    const match = this.check.exec(routingKey);
    if (!match) return [false, {}];
    return [true, { ...match.groups }];
  }
}

export interface Params { [key: string]: string }

export interface WithParams {
  params: Params;
}

export class Router<Message> {
  private readonly routes: Route<Message>[] = [];

  constructor(private readonly selector: (message: Message) => string) { }

  public use(path: Path, middleware: Middleware<Message>): void {
    this.routes.push(new Route(path, middleware));
  }

  public middleware = async (message: Message, next: Next<void>): Promise<void> => {
    const result = this.routes.map((route) => {
      const [match, params] = route.match(this.selector(message));
      return { match, params, middleware: route.middleware };
    }).find(({ match }) => match);

    if (!result) return next();

    const { params, middleware } = result;
    compose<Message & WithParams>(middleware)({ ...message, params }, next);
    return Promise.resolve();
  }
}
