// based on code from https://github.com/blakeembrey/compose-middleware
import { compose, Middleware, Next, RequestHandler } from './compose';

describe('compose middleware', () => {
  it('should be assignable to array of middleware', (done) => {
    const pipeline = <T, U>(...middlewareArray: Middleware<T, U>[]): RequestHandler<T, U> => compose(middlewareArray);
    const middleware = pipeline((_req: undefined, next: () => void) => next());

    return middleware(undefined, done);
  });

  it('should compose middleware', (done) => {
    const middleware = compose([
      (req: any, next: Next) => {
        req.one = true;
        next();
      },
      (req: any, next: Next) => {
        req.two = true;
        next();
      },
    ]);

    const req: any = {};

    middleware(req, (err) => {
      expect(err).toBeUndefined();
      expect(req.one).toEqual(true);
      expect(req.two).toEqual(true);

      return done();
    });
  });

  it('should compose async middleware', (done) => {
    const middleware = compose([
      (req: any): Promise<void> => {
        req.one = true;
        return Promise.resolve();
      },
      (req: any): Promise<void> => {
        req.two = true;
        return Promise.resolve();
      },
    ]);

    const req: any = {};

    middleware(req, (err) => {
      expect(err).toBeUndefined();
      expect(req.one).toEqual(true);
      expect(req.two).toEqual(true);

      return done();
    });
  });

  it('should exit with an error', (done) => {
    const middleware = compose([
      (req: any, next: Next) => {
        req.one = true;
        next(new Error('test'));
      },
      (req: any, next: Next) => {
        req.two = true;
        next();
      },
    ]);

    const req: any = {};

    middleware(req, (err) => {
      expect(err).toBeInstanceOf(Error);
      expect(req.one).toEqual(true);
      expect(req.two).toBeUndefined();

      return done();
    });
  });

  it('should short-cut handler with a single function', (done) => {
    const middleware = compose([
      (req: any, next: Next) => {
        req.one = true;
        next();
      },
    ]);

    const req: any = {};

    middleware(req, (err) => {
      expect(err).toBeUndefined();
      expect(req.one).toEqual(true);

      return done();
    });
  });

  it('should accept a single function', (done) => {
    const middleware = compose<any, any>((req: any, next: Next) => {
      req.one = true;
      next();
    });

    const req: any = {};

    middleware(req, (err?: Error | null) => {
      expect(err).toBeUndefined();
      expect(req.one).toEqual(true);

      return done();
    });
  });

  it('should noop with no middleware', (done) => {
    const middleware = compose([]);

    middleware({}, done);
  });

  it('should validate all handlers are functions', () => {
    expect(() => compose(['foo'] as any)).toThrow('Handlers must be a function');
  });

  it('should support error handlers', (done) => {
    const middleware = compose(
      (_req: any, next: Next) => {
        return next(new Error('test'));
      },
      (_: Error, _req: any, next: Next) => {
        return next();
      },
      (req: any, next: Next) => {
        req.success = true;
        return next();
      },
      (_: Error, req: any, next: Next) => {
        req.fail = true;
        return next();
      },
    );

    const req: any = {};

    middleware(req, (err) => {
      expect(req.fail).toBeUndefined();
      expect(req.success).toEqual(true);

      return done(err);
    });
  });

  it('should error when calling `next()` multiple times', (done) => {
    const middleware = compose<any, any>(
      (_req: any, next: Next) => {
        next();
        next();
      },
    );

    try {
      middleware({}, () => {/* */ });
    } catch (err) {
      expect(err.message).toEqual('`next()` called multiple times');

      return done();
    }
  });

  it('should forward thrown errors', (done) => {
    const middleware = compose<any, any>(
      (_req: any, _next: Next) => {
        throw new Error('Boom!');
      },
    );

    middleware({}, (err) => {
      expect(err).toBeInstanceOf(Error);
      expect(err?.message).toEqual('Boom!');

      return done();
    });
  });

  it('should not cascade errors from `done()`', (done) => {
    const request = {
      done: 0,
      first: 0,
      second: 0,
      third: 0,
    };

    const middleware = compose<typeof request, any>(
      (req: typeof request, next: Next) => {
        req.first++;

        return next();
      },
      (req: typeof request, _next: Next) => {
        req.second++;

        throw new TypeError('Boom!');
      },
      (req: typeof request, next: Next) => {
        req.third++;

        return next();
      },
    );

    try {
      middleware(request, () => {
        request.done++;

        throw new TypeError('This is the end');
      });
    } catch (err) {
      expect(request.done).toEqual(1);
      expect(request.first).toEqual(1);
      expect(request.second).toEqual(1);
      expect(request.third).toEqual(0);

      expect(err).toBeInstanceOf(TypeError);
      expect(err.message).toEqual('This is the end');

      return done();
    }

    return done(new TypeError('Missed thrown error'));
  });

  it('should avoid handling post-next thrown errors', (done) => {
    const middleware = compose<any, any>(
      (_req: any, next: Next) => {
        return next();
      },
      (_req: any, next: Next) => {
        next();
        throw new TypeError('Boom!');
      },
      (_req: any, next: Next) => {
        return setTimeout(next);
      },
    );

    try {
      middleware({}, (err) => {
        return done(err);
      });
    } catch (err) {
      expect(err).toBeInstanceOf(TypeError);
      expect(err.message).toEqual('Boom!');
      return;
    }

    return done(new TypeError('Missed thrown error'));
  });

  it('should compose functions without all arguments', (done) => {
    const middleware = compose<any, any>(
      (_req: any, next: Next) => {
        return next();
      },
      () => {
        return done();
      },
    );

    middleware({}, (err) => {
      return done(err || new Error('Middleware should not have finished'));
    });
  });
});
