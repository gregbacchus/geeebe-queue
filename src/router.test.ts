import { describeGiven, then, when } from '@geeebe/jest-bdd';
import { Message, MockMessage } from './mock/message';
import { Router } from './router';

const expectRouteToBeCalled = (middleware: jest.Mock<any, any>, deliveryTag: number): void => {
  then('the message will be routed to the middleware of the matching route', () => {
    expect(middleware).toBeCalled();
  });
  then('the middleware will be passed the message', () => {
    expect(middleware).toBeCalledWith(
      expect.objectContaining({
        fields: expect.objectContaining({
          deliveryTag,
        }),
      }),
      expect.anything(),
    );
  });
};

describe('Router', () => {
  describeGiven('a router is configured with a single route that accepts wildcards', () => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const given = () => {
      const middleware = jest.fn();
      const router = new Router<Message>((message) => message.fields.routingKey);
      router.use('test.*.bar', middleware);
      return { middleware, router };
    };

    when('a message is received that matches the wildcard', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('test.foo.bar');
      const next = jest.fn();
      router.middleware(message, next);
      expectRouteToBeCalled(middleware, message.fields.deliveryTag);
      then('the next() will not be called', () => {
        expect(next).not.toBeCalled();
      });
    });

    when('a message is received that does not match the wildcard', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('foo.123.bar');
      const next = jest.fn();
      router.middleware(message, next);
      then('the message will not be routed to the middleware', () => {
        expect(middleware).not.toBeCalled();
      });
      then('the next() will be called', () => {
        expect(next).toBeCalled();
      });
    });
  });

  describeGiven('a router is configured with a single parameterized route', () => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const given = () => {
      const middleware = jest.fn();
      const router = new Router<Message>((message) => message.fields.routingKey);
      // router.use('test.:p.bar', middleware);
      router.use(':id.test.:hello', middleware);
      return { middleware, router };
    };

    when('a message is received that matches the router', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('test.test.bar');
      const next = jest.fn();
      router.middleware(message as any, next);
      expectRouteToBeCalled(middleware, message.fields.deliveryTag);
      then('the middleware will be passed the parameters', () => {
        expect(middleware).toBeCalledWith(
          expect.objectContaining({
            params: expect.objectContaining({
              hello: 'bar',
              id: 'test',
            }),
          }),
          expect.anything(),
        );
      });
      then('the next() will not be called', () => {
        expect(next).not.toBeCalled();
      });
    });

    when('a message is received that does not match the route', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('foo.123.bar');
      const next = jest.fn();
      router.middleware(message as any, next);
      then('the message will not be routed to the middleware', () => {
        expect(middleware).not.toBeCalled();
      });
      then('the next() will be called', () => {
        expect(next).toBeCalled();
      });
    });
  });

  describeGiven('a router is configured with a single route with string path', () => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const given = () => {
      const middleware = jest.fn();
      const router = new Router<Message>((message) => message.fields.routingKey);
      // router.use('test.:p.bar', middleware);
      router.use('foo', middleware);
      return { middleware, router };
    };

    when('a message is received that matches the router', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('foo');
      const next = jest.fn();
      router.middleware(message, next);
      expectRouteToBeCalled(middleware, message.fields.deliveryTag);
      then('the next() will not be called', () => {
        expect(next).not.toBeCalled();
      });
    });

    when('a message is received that does not match the route', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('bar');
      const next = jest.fn();
      router.middleware(message, next);
      then('the message will not be routed to the middleware', () => {
        expect(middleware).not.toBeCalled();
      });
      then('the next() will be called', () => {
        expect(next).toBeCalled();
      });
    });
  });

  describeGiven('a router is configured with a single route with regex path', () => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const given = () => {
      const middleware = jest.fn();
      const router = new Router<Message>((message) => message.fields.routingKey);
      // router.use('test.:p.bar', middleware);
      router.use(/^foo.*$/, middleware);
      return { middleware, router };
    };

    when('a message is received that matches the router', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('foobar');
      const next = jest.fn();
      router.middleware(message, next);
      expectRouteToBeCalled(middleware, message.fields.deliveryTag);
      then('the next() will not be called', () => {
        expect(next).not.toBeCalled();
      });
    });

    when('a message is received that does not match the route', () => {
      const { middleware, router } = given();
      const message = MockMessage.empty('bar');
      const next = jest.fn();
      router.middleware(message, next);
      then('the message will not be routed to the middleware', () => {
        expect(middleware).not.toBeCalled();
      });
      then('the next() will be called', () => {
        expect(next).toBeCalled();
      });
    });
  });
});
