import { Service } from '@geeebe/service';
import { Middleware } from './compose';

export interface Connected {
  disconnect: () => Promise<void>;
}

export interface Listener {
  listen: (queue: string) => Promise<Connected>;
}

export interface ReceiveQueue<Options, Message> extends Service {
  readonly isConnected: boolean;
  listener(options?: Options): Promise<Listener>;
  listen(queue: string, options?: Options): Promise<Connected>;
  use(middleware: Middleware<Message>): void;
}

export interface SendQueue<Target, Options, Message = {}> extends Service {
  readonly isConnected: boolean;
  send(target: Target, message: Message, options?: Options): Promise<void>;
}
