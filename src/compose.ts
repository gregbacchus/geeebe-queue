// based on code from https://github.com/blakeembrey/compose-middleware
import flatten = require('array-flatten');

export type Next<T = void> = (err?: Error | null) => T;
export type RequestHandler<M, V = void> = (msg: M, next: Next<V>) => V;
export type AsyncRequestHandler<M> = (msg: M) => Promise<void>;
export type ErrorHandler<M, V = void> = (err: Error | null, msg: M, next: Next<V>) => V;
export type Middleware<M, V = void> = RequestHandler<M, V> | AsyncRequestHandler<M> | ErrorHandler<M, V>;

export interface NestedArray<T> extends ReadonlyArray<T | NestedArray<T>> { }
export type Handler<M, V = void> = Middleware<M, V> | NestedArray<Middleware<M, V>>;

/**
 * Compose an array of middleware handlers into a single handler.
 */
export const compose = <M, V = void>(...handlers: Handler<M, V>[]): RequestHandler<M, V> => {
  const middleware = generate(handlers);

  return (msg: M, done: Next<V>) => middleware(null, msg, done) as any;
};

/**
 * Wrap middleware handlers.
 */
export const errors = <M, V = void>(...handlers: Handler<M, V>[]): ErrorHandler<M, V> => {
  return generate(handlers);
};

/**
 * Generate a composed middleware function.
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const generate = <M, V = void>(handlers: Handler<M, V>[]) => {
  const stack = flatten<Handler<M, V>>(handlers);

  for (const handler of stack) {
    if (typeof handler as any !== 'function') {
      throw new TypeError('Handlers must be a function');
    }
  }

  const middleware = (err: Error | null, msg: M, done: Next<V>): V => {
    let index = -1;

    const dispatch = (pos: number, err?: Error | null): V => {
      const handler = stack[pos];

      index = pos;

      if (index === stack.length) return done(err);

      const next = (err?: Error | null): V => {
        if (pos < index) {
          throw new TypeError('`next()` called multiple times');
        }

        return dispatch(pos + 1, err);
      };

      try {
        if (handler.length === 3) {
          if (err) {
            return (handler as ErrorHandler<M, V>)(err, msg, next);
          }
        } else {
          if (!err) {
            const result = (handler as RequestHandler<M, V>)(msg, next) as any;
            if (result && typeof result.then === 'function') {
              result.then(() => next()).catch((error: Error) => next(error));
            }
            return result;
          }
        }
      } catch (e) {
        // Avoid future errors that could diverge stack execution.
        if (index > pos) throw e;

        return next(e);
      }

      return next(err);
    };

    return dispatch(0, err);
  };

  return middleware;
};
